# -*- coding: utf-8 -*-
import scrapy
from bs4 import BeautifulSoup
from tutorial.items import QuoteItem

'''
这里有三个属性和一个方法
name :它是每个项目唯一的名字，用来区分不同的spider
allowed_domains:允许爬取的域名，如果初始或者后续的请求链接不是这个域名下的，则请求链接会被过滤掉
start_urls:包含了spider在启动时爬取的url列表，初始请求时又它来定义的,在没有实现start_requests()方法时，默认从这个列表开始抓取

parsescr():spider的一个方法，默认情况下，被调用时start_urls里面的链接构成的请求完成下载执行后，返回的响应就会作为唯一的参数传递给这个函数
该方法负责解析返回的响应，提取数据或者进一步生成要处理的请求链接。
'''
class QuotesSpider(scrapy.Spider):

    name = 'quotes'
    allowed_domains = ['quotes.toscrape.com']
    start_urls = ['http://quotes.toscrape.com/']

    def parse(self, response):
        quotes = BeautifulSoup(response.text,'lxml')
        for quote in quotes.find_all(name = 'div',class_='quote'):
            item = QuoteItem() #使用items中定义的数据结构
            for s in quote.find_all(name = 'span',class_='text'):
                item['text'] = s.text
            for s in quote.find_all(name= 'small',class_='author'):
                item['author'] = s.text
            for s in quote.find_all(name='div', class_='tags'):
                item['tags'] = s.text.replace('\n','').strip().replace(' ','')
            yield item

        nexts = quotes.find_all(name='li', class_='next')
        for next in nexts:
            n = next.find(name='a')
            url = 'http://quotes.toscrape.com/' + n['href']
            yield scrapy.Request(url = url,callback = self.parse)
