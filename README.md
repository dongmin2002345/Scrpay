此项目为python爬虫项目代码。
1、mymovie.py
    爬取猫眼电影排行榜前100名电影数据

2、others.py
    在代码开发过程中的临时代码
    
3、tool.py
    为其他模块封装的公共方法，提供支持使用
 
4、weatherDate.py
    爬取历史天气数据

5、WeiboAjax.py
    爬取ajax页面数据
 
6、zhaopinData.py
    爬取拉钩网招聘数据