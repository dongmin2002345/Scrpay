from db import RedisClient
import GetProxy


POOL_UPPER_LIMIT = 10000
class Getter():
    '''
    有效代理池维护，定于代理池最大上线，当代理池中代理数量超过上限时，不在添加代理
    '''
    def __init__(self):
        self.redis = RedisClient() #连接redis数据库，操作redis数据库的实例
        self.crawler = GetProxy.Crawler()  #获取代理的实例

    def is_over_threshold(self):
        '''
        判断是否达到了代理池的上线
        '''
        if self.redis.count() >= POOL_UPPER_LIMIT:
            return True
            print('count: ', self.redis.count())
        else:
            print('count: ', self.redis.count())
            False

    def run(self):
        print('代理获取器开始执行')
        if not self.is_over_threshold():
            xici_proxys = self.crawler.crawl_xici()
            for proxy in xici_proxys:
                self.redis.add(proxy)
            ip66_proxys = self.crawler.crawl_66ip()
            for proxy in ip66_proxys:
                self.redis.add(proxy)
        else:
            pass

# if __name__ == '__main__':
#     getter = Getter()
#     getter.run()