import requests

proxy_pool_url = 'http://localhost:5000/random'

def get_proxy():
    try:
        response = requests.get(proxy_pool_url)
        if response.status_code == 200:
            return response.text
    except ConnectionError:
        return None

def test_proxy():
    proxy = get_proxy()
    print(proxy)
    proxies = {
        'http':'http://' + proxy,
        'https': 'https://' + proxy,
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
        'Connection': 'close'
    }
    url = 'http://httpbin.org/get'
    try:
        response = requests.get(url = url,headers = headers,proxies=proxies)
        print(response.text)
    except requests.exceptions.ConnectionError as e:
        print('Error :'  +e.args)


if __name__=='__main__':
    test_proxy()
